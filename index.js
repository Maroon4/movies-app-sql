const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const PeopleRoutes = require('./routes/people');
// const  db = require('./db');
const db = require('./config/db.config');
const movieRouter = require('./routes/movie-router');
const usersRouter = require('./routes/users-router');
const serialRouter = require('./routes/serial-router');


const app = express();
const apiPort = 3000;




app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());
app.use(bodyParser.json());


// db.on('error', console.error.bind(console, "Mysql connection error:"));

//
const Role = db.role;

// force: true will drop the table if it already exists
// db.sequelize.sync({force: false}).then(() => {
//     console.log('Drop and Resync with { force: false }');
//     // initial();
// });

app.use('/people', PeopleRoutes);


app.get('/', (req, res) => {
    res.send('Server on')
});

app.use('/api', movieRouter);
app.use('/api', usersRouter);
app.use('/api', serialRouter);

// app.listen(apiPort, () => console.log(`Server running on port ${apiPort}`));

var server = app.listen(apiPort, function () {
    var host = server.address().address;
    var port = server.address().address;
   console.log("App listen at http://%s:%s", host, port)
});

function initial(){
    Role.create({
        id: 1,
        name: "USER"
    });

    Role.create({
        id: 2,
        name: "ADMIN"
    });

    Role.create({
        id: 3,
        name: "PM"
    });
}