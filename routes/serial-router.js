const express = require('express');

const router = express.Router();

const SerialsController = require('../controllers/serial-ctrl');

router.post('/serial', SerialsController.createSerial);
router.put('/serial/:id', SerialsController.updateSerial);
router.delete('/serial/:id', SerialsController.deleteSerial);
router.get('/serial/:id', SerialsController.findById);
router.get('/series', SerialsController.getAllSeries);

module.exports = router;