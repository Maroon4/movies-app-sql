const express = require('express');
const verifySignUp = require('./verifySignUp');
const authJwt = require('./verifyJwtToken');

const router = express.Router();

    const UsersController = require('../controllers/controller.js');

    router.post('/auth/signup', [verifySignUp.checkDuplicateUserNameOrEmail, verifySignUp.checkRolesExisted], UsersController.signup);

    router.post('/auth/signin', UsersController.signin);

    router.get('/test/user', [authJwt.verifyToken], UsersController.userContent);

    router.get('/test/pm', [authJwt.verifyToken, authJwt.isPmOrAdmin], UsersController.managementBoard);

    router.get('/test/admin', [authJwt.verifyToken, authJwt.isAdmin], UsersController.adminBoard);

module.exports = router;
