const  express = require('express');
const  db = require('../db');

const router = express.Router();

router.get("/", (req, res) => {
    db.query("SELECT * from people", (err, rows, fields) => {
        if (!err)
        {
            res.send(rows);
        }
        else {
            console.log(err);
        }
    })
});

module.exports = router;