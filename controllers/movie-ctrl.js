
const db = require('../config/db.config');
const Movie = db.movie;


// const Movie = require('../models/movie-model');


createMovie = (req, res) => {

    const body = req.body;

    console.log(body);

    if (!body) {
        return res.status(400).json({
            success: false,
            error: 'You must provide a movie',
        })
    }

    Movie.create({
        name: req.body.name,
        time: req.body.time,
        rating: req.body.rating,

    }).then(() => {
        return res.status(201).json({
            success: true,
            message: "Movie creat successfully!"
        })
    }).catch(err => {
        res.status(500).json({
            success: false,
            error: err
        });
    })
};


updateMovie = async (req, res) => {
    const body = req.body;

    if (!body) {
        return res.status(400).send("You must provide a movie")
    }

    Movie.update({
            name: req.body.name,
            time: req.body.time,
            rating: req.body.rating,

        }, {
            where: {id: req.params.id},
            returning: true
        },
    ).then(() => {
        return res.status(200).json({
            success: true,
            id: req.params.id,
            message: 'Movie update!'
        })
    })
        .catch(err => {
            res.status(404).json({
                err,
                message: 'Movie not update!'
            })
        })


};

deleteMovie = async (req, res) => {

    Movie.destroy({
            where: {id: req.params.id},
        },
    ).then(() => {
        return res.status(200).json({
            success: true,
            id: req.params.id,
            message: 'Movie deleted!'
        })
    })
        .catch(err => {
            res.status(404).json({
                err,
                message: 'Movie not deleted!'
            })
        })
};

getMovieById = async (req, res) => {

    Movie.findOne({
        where: {id: req.params.id},
        attributes: ['name', 'time', 'rating']
    }).then((movie) => {
        console.log(`Дані з бази ${movie}`);

        res.status(200).json({
            description: "Movie data",
            data: movie
        })
    }).catch((err) => {
        res.status(500).json({
            description: "Can not show serial",
            error: err
        })
    })

};



getMovies = async (req, res) => {

    try {
        await Movie.findAll()
            .then(movies => {

                console.log(`Прийшло ${movies}`);

                res.status(200).json({
                    description: "Movies data",
                    data: movies
                })
            }).catch(err => {
                res.status(500).json({
                    description: "Can not show movies",
                    error: err
                })
            });
    }
    catch (err) {
        err => console.log(err)
    }

};

module.exports = {
    createMovie,
    updateMovie,
    deleteMovie,
    getMovies,
    getMovieById,
};
