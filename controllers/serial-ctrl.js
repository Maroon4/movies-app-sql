const db = require('../config/db.config');
const config = require('../config/config');
const Serial = db.serial;

exports.createSerial = (req, res) => {

    const body = req.body;

    console.log(body);

    if (!body) {
        return res.status(400).json({
            success: false,
            error: 'You must provide a serial',
        })
    }

    Serial.create({
        name: req.body.name,
        time: req.body.time,
        rating: req.body.rating,
        sesons: req.body.sesons
    }).then(() => {
        return res.status(201).json({
            success: true,
            message: "Serial creat successfully!"
        })
    }).catch(err => {
        res.status(500).json({
            success: false,
            error: err
        });
    })
};

exports.updateSerial = (req, res) => {

    const body = req.body;

    if (!body) {
        return res.status(400).send("You must provide a serial")
    }

    Serial.update({
            name: req.body.name,
            time: req.body.time,
            rating: req.body.rating,
            sesons: req.body.sesons
        }, {
        where: {id: req.params.id},
        returning: true
        },
    ).then(() => {
                    return res.status(200).json({
                        success: true,
                        id: req.params.id,
                        message: 'Serial update!'
                    })
                })
                .catch(err => {
                    res.status(404).json({
                        err,
                        message: 'Serial not update!'
                    })
                })

};

exports.deleteSerial = (req, res) => {

    Serial.destroy({
            where: {id: req.params.id},
        },
    ).then(() => {
        return res.status(200).json({
            success: true,
            id: req.params.id,
            message: 'Serial deleted!'
        })
    })
        .catch(err => {
            res.status(404).json({
                err,
                message: 'Serial not deleted!'
            })
        })


};

exports.findById = (req, res) => {

    Serial.findOne({
        where: {id: req.params.id},
        attributes: ['name', 'time', 'rating', 'sesons']
    }).then((serial) => {

       console.log(serial);

        res.status(200).json({
            description: "Serial data",
            data: serial
        })

    }).catch((err) => {
            res.status(500).json({
                description: "Can not show serial",
                error: err
            })
        })

};

exports.getAllSeries = async (req, res) => {

    await Serial.findAll()
        .then(series => {

            console.log(`Прийшло ${series}`);

            res.status(200).json({
                description: "Series data",
                data: series
            })
        }).catch(err => {
             res.status(500).json({
                  description: "Can not show series",
                  error: err
        })
    });
    // console.log(`Прийшло ${res}`)
};