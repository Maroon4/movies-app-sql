
module.exports = (sequelize, Sequelize) => {

    const Movie = sequelize.define('movies', {
        name: {
            type: Sequelize.STRING
        },
        time: {
            type: Sequelize.STRING
        },
        rating: {
            type: Sequelize.STRING
        },
    });

    return Movie

};

// module.exports = Movie;


