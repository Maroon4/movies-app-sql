
module.exports = (sequelize, Sequelize) => {
    const Serial = sequelize.define('series', {
        name: {
            type: Sequelize.STRING
        },
        time: {
            type: Sequelize.STRING
        },
        rating: {
            type: Sequelize.STRING
        },
        sesons: {
            type: Sequelize.STRING
        }
    });

    return Serial
};